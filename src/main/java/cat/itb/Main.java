package cat.itb;

import cat.itb.Database.MyMongoDatabase;
import cat.itb.Utilities.Tools.Menu;
import cat.itb.Utilities.Tools.Menu.MenuOption;

public class Main
{
    public static final String VIRTUAL_MACHINE_IP = "192.168.56.101", JSONS_FILES_PATH = "src/main/resources/JSONS/";
    public static final String DATABASE_NAME = "itb", COLLECTION_NAME_BOOKS = "books", COLLECTION_NAME_COUNTRIES = "countries";

    public Menu createMenu()
    {
        MyMongoDatabase mongo = new MyMongoDatabase(VIRTUAL_MACHINE_IP);

        //Creation of databases
        mongo.loadArrayBooks(DATABASE_NAME, COLLECTION_NAME_BOOKS, JSONS_FILES_PATH + "ArrayBooks.json");
        mongo.loadArrayCountries(DATABASE_NAME, "countries", JSONS_FILES_PATH + "countries.json");

        Menu menu = new Menu("Please, select an option: ", "There's no such option!", "Welcome to my program", "goodbye :D!", true,

                //----------------------- Searches ----------------------------
                new MenuOption("Show long description.", () -> mongo.showField("longDescription", DATABASE_NAME, COLLECTION_NAME_BOOKS), "ld"),
                new MenuOption("Show Danno Ferring books.", () -> mongo.showBooksOf("Danno Ferrin"), "df"),
                new MenuOption("Show page count of books with category java and between 300/350 pages.", () -> mongo.showBooksPagesBtwAndCategory(300, 350, "Java"), "pc"),
                new MenuOption("Show books of Charlie Collins and Robi Sen", () -> mongo.showBooksOfAuthors(new String[]{"Charlie Collins", "Robi Sen"}), "ba"),
                new MenuOption("Show books of category Java, except for author Vikram Goyal.", () -> mongo.showBooksOfCategoriesExceptForAuthor(new String[]{"Java"}, "Vikram Goyal"), "ca"),
                new MenuOption("Show books of categories mobile and java, with a limit of 15", () -> mongo.showBooksOfCategories(new String[]{"Mobile", "Java"}, 15), "bc"),

                //----------------------- Updates ----------------------------
                new MenuOption("Set assestment of PUBLISH books of category Internet to 5000.", () -> System.out.println("Modified: " + mongo.setAssesment(5000) + " books."), "sa"),
                new MenuOption("Add author Roger Whatch to book of title Unlocking Android.", () -> System.out.println("Modified: " + mongo.addAuthor("Roger Whatch", "Unlocking Android") + " books."), "aa"),
                new MenuOption("Create unique index key using isbn, and catch error.", () -> mongo.createUniqueKeyIndex("isbn"), "ui"),
                new MenuOption("Show list of all indexes.", mongo::showAllIndex, "si"),

                //----------------------- Aggregations ----------------------------
                new MenuOption("See count of english speaking countries.", () -> mongo.getCountOfEnglishSpeakingCountries(DATABASE_NAME, COLLECTION_NAME_COUNTRIES), "ec"),
                new MenuOption("Check if africa is the region with more countries.", () -> mongo.checkIfRegionWithMoreCountriesIsAfrica(DATABASE_NAME, COLLECTION_NAME_COUNTRIES), "ra"),
                new MenuOption("Create collection of 7 largest countries and check if USA is there.", () -> mongo.createLargest7CountriesCollection(DATABASE_NAME, COLLECTION_NAME_COUNTRIES), "7c"),
                new MenuOption("See country with more frontier neighbors", () -> mongo.seeCountryWithMoreNeighbors(DATABASE_NAME, COLLECTION_NAME_COUNTRIES), "mn")
        );

        menu.setOnDestroyAction(() ->
        {
            //Deletion of databases
            mongo.dropCollection(DATABASE_NAME, COLLECTION_NAME_COUNTRIES);
            mongo.dropCollection(DATABASE_NAME, "largest_seven");
            mongo.dropBooks();
        });

        return menu;
    }

    public static void main(String[] args)
    {
        Main program = new Main();

        program.createMenu().startMenu();
    }
}
