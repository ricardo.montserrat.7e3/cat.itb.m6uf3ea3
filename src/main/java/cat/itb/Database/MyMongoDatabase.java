package cat.itb.Database;

import cat.itb.POJO.Book.Book;
import cat.itb.POJO.Country.Country;
import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MyMongoDatabase
{
    public static final String MONGO_BASE_PORT = ":27017";

    private final MongoClient client;

    public MyMongoDatabase(final String ip)
    {
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        client = new MongoClient(new ServerAddress(ip + MONGO_BASE_PORT), MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
    }

    //region COLLECTION_METHODS
    public void loadArrayJson(final String database, final String collection, final String filePath)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {
            stringFile = br.lines().collect(Collectors.joining());
        }
        catch (Exception e) { System.out.println(e.toString()); }

        getCollection(database, collection).insertMany(Arrays.asList(new Gson().fromJson(stringFile, Document[].class)));
    }

    public MongoCollection<Document> getCollection(final String database, final String collection) { return client.getDatabase(database).getCollection(collection); }

    public void showCollections(final String database) { client.getDatabase(database).listCollectionNames().forEach((Consumer<? super String>) System.out::println); }

    public void showCollectionIndexes(final String database, final String collection) { getCollection(database, collection).listIndexes().forEach((Consumer<? super Document>) System.out::println);}

    public long dropCollection(final String database, final String collection)
    {
        MongoCollection<Document> toDestroy = client.getDatabase(database).getCollection(collection);
        long collectionSize = toDestroy.countDocuments();
        toDestroy.drop();
        return collectionSize;
    }

    public void createIndex(final String database, final String collection, final String index, final boolean unique)
    {
        try { getCollection(database, collection).createIndex(Indexes.text(index), new IndexOptions().unique(unique)); }
        catch (DuplicateKeyException e)
        {
            System.out.printf("The index \"%s\" was duplicated in a field of the database \"%s\" inside the collection \"%s\"; so it couldn't be created!", index, database, collection);
        }
    }

    //endregion ---------------------------------------------------------------------------------

    //region SEARCH_METHODS

    //region BASE_METHODS
    public void showField(final String field, final String database, final String collection)
    {
        getCollection(database, collection).find().projection(Projections.fields(Projections.include(field), Projections.excludeId())).forEach((Consumer<? super Document>) System.out::println);
    }

    public void showDocuments(final String database, final String collection, Bson filter, int limit, Bson sortType, Bson projection)
    {
        getCollection(database, collection).find(filter).limit(limit).sort(sortType).projection(projection).forEach((Consumer<? super Document>) System.out::println);
    }
    //endregion ---------------------------------------------------------------------------------

    public void showDocumentsWhere(final String field, final String[] valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.all(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhereHas(final String field, final String[] valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.in(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhere(final String field, final String valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.eq(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhereBetween(final String field, final double minValueField, final double maxValueField, final String database, final String collection, final Bson optionalFilter, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Updates.combine(Filters.gte(field, minValueField), Filters.lte(field, maxValueField)), optionalFilter),
                0,
                Sorts.ascending(),
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }
    //endregion ---------------------------------------------------------------------------------

    //region UPDATE_METHODS

    //region BASE_METHODS
    public boolean updateDocument(final String database, final String collection, final Bson filter, final Bson update)
    {
        return getCollection(database, collection).updateOne(filter, update).wasAcknowledged();
    }

    public long updateDocuments(final String database, final String collection, final Bson filter, final Bson update)
    {
        return getCollection(database, collection).updateMany(filter, update).getModifiedCount();
    }

    public void insertDocument(final String database, final String collection, Document document) { getCollection(database, collection).insertOne(document); }

    public void insertDocuments(final String database, final String collection, List<Document> documents) { getCollection(database, collection).insertMany(documents); }

    public long deleteDocuments(final String database, final String collection, final Bson filter)
    {
        return getCollection(database, collection).deleteMany(filter).getDeletedCount();
    }
    //endregion ---------------------------------------------------------------------------------

    public long deleteDocumentsWhereBetween(final String database, final String collection, final String field, int minValue, int maxValue)
    {
        return deleteDocuments(database, collection, Updates.combine(Filters.gte(field, minValue), Filters.lte(field, maxValue)));
    }

    public long updateDocumentsWhereBetween(final String database, final String collection, final String fieldToCompare, int minValue, int maxValue, final String fieldToChange, int newValue)
    {
        return updateDocuments(database, collection, Updates.combine(Filters.gte(fieldToCompare, minValue), Filters.lte(fieldToCompare, maxValue)), Updates.set(fieldToChange, newValue));
    }

    public long setField(final String field, final String value, final String database, final String collection) { return updateDocuments(database, collection, Filters.and(), Updates.set(field, value)); }

    public long addToField(final String field, final String value, final String database, final String collection) { return updateDocuments(database, collection, Filters.and(), Updates.addToSet(field, value)); }

    //endregion ---------------------------------------------------------------------------------

    //region PRACTICE_SPECIFIC_METHODS
    public void loadArrayBooks(final String database, final String collection, final String filePath)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {
            stringFile = br.lines().collect(Collectors.joining());
        }
        catch (Exception e) { System.out.println(e.toString()); }

        MongoCollection<Book> collectionPeople = client.getDatabase(database).getCollection(collection, Book.class);
        collectionPeople.insertMany(Arrays.asList(new Gson().fromJson(stringFile, Book[].class)));
    }

    public void loadArrayCountries(final String database, final String collection, final String filePath)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {
            stringFile = br.lines().collect(Collectors.joining());
        }
        catch (Exception e) { System.out.println(e.toString()); }

        MongoCollection<Country> collectionPeople = client.getDatabase(database).getCollection(collection, Country.class);
        collectionPeople.insertMany(Arrays.asList(new Gson().fromJson(stringFile, Country[].class)));
    }

    public void dropBooks() { dropCollection("itb", "books"); }

    public void showBooksOf(String author)
    {
        showDocumentsWhere("authors", author, "itb", "books", 0, Filters.and(), Sorts.ascending(), "title", "authors");
    }

    public void showBooksPagesBtwAndCategory(final double minPageCount, final double maxPageCount, final String category)
    {
        showDocumentsWhereBetween("pageCount", minPageCount, maxPageCount, "itb", "books", Filters.eq("categories", category), "title", "authors", "categories");
    }

    public void showBooksOfAuthors(final String[] authors)
    {
        showDocumentsWhere("authors", authors, "itb", "books", 0, Filters.and(), Sorts.ascending(), "title", "authors");
    }

    public void showBooksOfCategoriesExceptForAuthor(final String[] categories, final String author)
    {
        showDocumentsWhere("categories", categories, "itb", "books", 0, Filters.ne("authors", author), Sorts.ascending("title"));
    }

    public void showBooksOfCategories(final String[] categories, int limit)
    {
        showDocumentsWhereHas("categories", categories, "itb", "books", limit, Filters.and(), Sorts.ascending("title"));
    }

    public long setAssesment(int value)
    {
        System.out.println("Updating assesment value of books with category Internet and status PUBLISHED to: " + value);
        return updateDocuments("itb", "books",
                Filters.and(Filters.eq("categories", "Internet"), Filters.eq("status", "PUBLISH")),
                Updates.set("Assesment", value));
    }

    public long addAuthor(final String author, final String title)
    {
        System.out.println("Adding author " + author + " to book of title " + title);
        return updateDocuments("itb", "books", Filters.eq("title", title), Updates.addToSet("authors", author));
    }

    public void createUniqueKeyIndex(final String key) { createIndex("itb", "books", key, true); }

    public void showAllIndex() { showCollectionIndexes("itb", "books"); }

    public void getCountOfEnglishSpeakingCountries(final String database, final String collection)
    {
        Document englishSpeakingCountries = getCollection(database, collection).aggregate(Arrays.asList(Aggregates.match(Filters.eq("languages.name", "English")), Aggregates.count())).first();
        assert englishSpeakingCountries != null;
        System.out.println("English speaking countries: " + englishSpeakingCountries.get("count"));
    }

    public void checkIfRegionWithMoreCountriesIsAfrica(final String database, final String collection)
    {
        Document maxCountriedRegion = getCollection(database, collection).aggregate(Arrays.asList(Aggregates.group("$region", Accumulators.sum("totalCount", 1)), Aggregates.sort(Sorts.descending("totalCount")))).first();

        assert maxCountriedRegion != null;
        System.out.println("Is Africa the region with more countries? " + maxCountriedRegion.containsValue("Africa"));
    }

    public void createLargest7CountriesCollection(final String database, final String collection)
    {
        getCollection(database, collection).aggregate(Arrays.asList(
                Aggregates.sort(Sorts.descending("area")),
                Aggregates.limit(7),
                Aggregates.out("largest_seven"))).toCollection();

        MongoCollection<Document> largestSeven = getCollection(database, "largest_seven");

        System.out.println("Are the countries limited to 7? " + (largestSeven.countDocuments() == 7));

        Document usa = largestSeven.find(Filters.eq("alpha3Code", "USA")).first();

        System.out.println("Is USA in the largest seven? " + (usa != null));
    }

    public void seeCountryWithMoreNeighbors(final String database, final String collection)
    {
        Bson borderingCountriesCollection = Aggregates.project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        MongoCollection<Document> col = getCollection(database, collection);
        int maxValue = Objects.requireNonNull(
                col.aggregate(Arrays.asList(borderingCountriesCollection,
                Aggregates.group(null, Accumulators.max("max", "$borderingCountries"))))
                .first()).getInteger("max");

        System.out.println("Is the max value 15? " + (maxValue == 15));

        Document maxNeighboredCountry = col.aggregate(Arrays.asList(borderingCountriesCollection,
                Aggregates.match(Filters.eq("borderingCountries", maxValue)))).first();

        assert maxNeighboredCountry != null;
        System.out.println("Is China the max neighbored country? " + maxNeighboredCountry.containsValue("China"));
    }

    //endregion ---------------------------------------------------------------------------------
}
