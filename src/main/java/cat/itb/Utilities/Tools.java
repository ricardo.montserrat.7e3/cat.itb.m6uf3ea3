package cat.itb.Utilities;

import java.util.*;

public class Tools
{
    public static void pause()
    {
        System.out.print("\n\nPress enter to continue...");
        new Scanner(System.in).nextLine();
    }

    public static double inputDouble(final String message, final boolean canBeNegative)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        double userNum = 0;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextDouble();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0 && !canBeNegative);
        return userNum;
    }

    public static int inputInt(final String message, final boolean canBeNegative)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        int userNum = 0;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextInt();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0 && !canBeNegative);
        return userNum;
    }


    public static String inputAny(final String message)
    {
        System.out.print(message);
        return new Scanner(System.in).nextLine();
    }

    public static class Menu
    {
        private final List<MenuOption> menuOptions = new ArrayList<>();
        private String selectionMessage, errorMessage, welcomeMessage, goodbyeMessage;
        private boolean lowerCaseInput;

        private MenuAction onDestroyAction;

        public Menu(final String selectionMessage, final String errorMessage, final String welcomeMessage, final String goodbyeMessage, final boolean lowerCaseInput)
        {
            this.selectionMessage = selectionMessage;
            this.errorMessage = errorMessage;
            this.welcomeMessage = welcomeMessage;
            this.goodbyeMessage = goodbyeMessage;
            this.lowerCaseInput = lowerCaseInput;
        }

        public Menu(final String selectionMessage, final String errorMessage, final String welcomeMessage, final String goodbyeMessage, final boolean lowerCaseInput, final MenuOption... menuOptions)
        {
            this.selectionMessage = selectionMessage;
            this.errorMessage = errorMessage;
            this.welcomeMessage = welcomeMessage;
            this.goodbyeMessage = goodbyeMessage;
            this.lowerCaseInput = lowerCaseInput;
            Collections.addAll(this.menuOptions, menuOptions);
            setMenuOptionsIndexes();
        }

        public void addMenuOptions(final MenuOption... menuOptions)
        {
            Collections.addAll(this.menuOptions, menuOptions);
            setMenuOptionsIndexes();
        }

        public void removeMenuOptions(final MenuOption... menuOptions)
        {
            for (MenuOption option : menuOptions) this.menuOptions.remove(option);
            setMenuOptionsIndexes();
        }

        private void setMenuOptionsIndexes()
        {
            int counter = 1;
            for (MenuOption option : menuOptions) option.index = counter++;
        }

        public void startMenu()
        {
            boolean finish;
            do
            {
                showMenu();
                finish = getInputMenu(Tools.inputAny(selectionMessage));
            }
            while (!finish);
            if(onDestroyAction != null) onDestroyAction.doAction();
            System.out.println(goodbyeMessage);
        }

        private boolean getInputMenu(String input)
        {
            if (lowerCaseInput) input = input.toLowerCase();

            if (input.equals("exit") || input.equals(String.valueOf(menuOptions.size() + 1))) return true;

            for (MenuOption option : menuOptions)
            {
                if (option.menuCommands.contains(input) || input.equals(String.valueOf(option.index)))
                {
                    if (option.action != null) option.action.doAction();
                    else option.actionParameter.doAction();
                    pause();
                    return false;
                }
            }
            System.out.println(errorMessage);
            return false;
        }

        private void showMenu()
        {
            //Jump to clear screen
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
            System.out.println("========== " + welcomeMessage + " ==========\n\nID, [Commands] .- Description");
            menuOptions.forEach(System.out::println);
            System.out.println((menuOptions.size() + 1) + ", [exit] .- Exit.\n\n");
        }

        public void setSelectionMessage(final String selectionMessage) { this.selectionMessage = selectionMessage; }

        public void setErrorMessage(final String errorMessage) { this.errorMessage = errorMessage; }

        public void setWelcomeMessage(final String welcomeMessage) { this.welcomeMessage = welcomeMessage; }

        public void setGoodbyeMessage(final String goodbyeMessage) { this.goodbyeMessage = goodbyeMessage; }

        public void setLowerCaseInput(final boolean lowerCaseInput) { this.lowerCaseInput = lowerCaseInput; }

        public void setOnDestroyAction(MenuAction onDestroyAction) { this.onDestroyAction = onDestroyAction; }

        public static class MenuOption
        {
            public int index;
            public String optionName;
            public HashSet<String> menuCommands = new HashSet<>();

            public MenuAction action;
            public MenuActionParameter actionParameter;

            public MenuOption(final String optionName, final MenuAction action, final String... menuCommands)
            {
                this.optionName = optionName;
                this.action = action;
                this.menuCommands.addAll(Arrays.asList(menuCommands));
            }

            public MenuOption(final String optionName, final MenuActionParameter action,final String... menuCommands)
            {
                this.optionName = optionName;
                this.actionParameter = action;
                this.menuCommands.addAll(Arrays.asList(menuCommands));
            }

            @Override
            public String toString() { return index + ", " + menuCommands + " .- " + optionName; }

            @Override
            public boolean equals(final Object o)
            {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                MenuOption option = (MenuOption) o;
                return index == option.index && Objects.equals(optionName, option.optionName) && Objects.equals(menuCommands, option.menuCommands);
            }

            @Override
            public int hashCode() { return Objects.hash(index, optionName, menuCommands); }
        }

        public interface MenuAction
        {
            void doAction();
        }

        public interface MenuActionParameter
        {
            void doAction(Object... objects);
        }
    }

    public static void main(String[] args)
    {
        //---------------------------------- Basic parameters ----------------------------------
        final String
                selectionMessage = "Please, select an option: ",
                errorMessage = "There's no such option",
                welcomeMessage = "Welcome to my program",
                goodbyeMessage = "Goodbye, thank you for using my program :D";

        //Whether you want to mind if the user writes capital letters or not
        final boolean lowerCaseInput = true;

        Menu menuTest = new Menu(selectionMessage, errorMessage, welcomeMessage, goodbyeMessage, lowerCaseInput);

        //--------------------------------------------------------------------------------------------------
        //---------------------------------- Creating Menu options -------------------------------------------

        //---------------------------------- Basic Menu Option parameters ----------------------------------
        final String menuTitle = "My menu option 1.", menuTitle2 = "My menu option 2", menuTitle3 = "My menu option 3\n\n";
        final Menu.MenuAction menuAction = () -> System.out.println("You selected action number 1!");

        Menu.MenuOption menuOption = new Menu.MenuOption(menuTitle, menuAction);

        //---------------------------------- Optional! Menu Option parameters ----------------------------------
        //You can add as many optional commands as you want, so this option can be called with different words
        Menu.MenuOption menuOption3 = new Menu.MenuOption(menuTitle3, menuAction, "n3", "n");

        Menu.MenuOption[] menuOptions = new Menu.MenuOption[]
                {
                        menuOption,
                        new Menu.MenuOption(menuTitle2, () -> System.out.println("You selected 2!")),
                        menuOption3
                };

        //--------------------------------------------------------------------------------------------------
        //---------------------------------- Adding Menu options to the Menu -------------------------------------------

        menuTest.addMenuOptions(menuOptions);

        menuTest.addMenuOptions(
                new Menu.MenuOption("Direct Injection", () -> System.out.println("!!!")),
                new Menu.MenuOption("Of menu options!", () -> System.out.println("???")),
                new Menu.MenuOption("Without creating double arrays!", () -> System.out.println("..."))
        );

        //--------------------------------------------------------------------------------------------------
        //---------------------------------- Using the Menu -------------------------------------------

        //---------------------------------- Optional! Setting an OnDestroyMethod ----------------------------------
        //This method will be called once the menu instance is over!
        menuTest.setOnDestroyAction(() -> System.out.println("------------------ Look at me I'm being destroyed!!! -------------------"));

        //And to start the menu simply use this
        menuTest.startMenu();
    }

}

