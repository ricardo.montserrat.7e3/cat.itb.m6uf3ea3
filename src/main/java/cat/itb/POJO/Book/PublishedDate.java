package cat.itb.POJO.Book;

import com.google.gson.annotations.SerializedName;

public class PublishedDate{

	@SerializedName("$date")
	private String date;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	@Override
 	public String toString(){
		return 
			"PublishedDate{" + 
			"$date = '" + date + '\'' + 
			"}";
		}
}