package cat.itb.POJO.Book;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Book {

	@SerializedName("longDescription")
	private String longDescription;

	@SerializedName("pageCount")
	private int pageCount;

	@SerializedName("isbn")
	private String isbn;

	@SerializedName("_id")
	private int id;

	@SerializedName("publishedDate")
	private PublishedDate publishedDate;

	@SerializedName("shortDescription")
	private String shortDescription;

	@SerializedName("categories")
	private List<String> categories;

	@SerializedName("title")
	private String title;

	@SerializedName("thumbnailUrl")
	private String thumbnailUrl;

	@SerializedName("status")
	private String status;

	@SerializedName("authors")
	private List<String> authors;

	public void setLongDescription(String longDescription){
		this.longDescription = longDescription;
	}

	public String getLongDescription(){
		return longDescription;
	}

	public void setPageCount(int pageCount){
		this.pageCount = pageCount;
	}

	public int getPageCount(){
		return pageCount;
	}

	public void setIsbn(String isbn){
		this.isbn = isbn;
	}

	public String getIsbn(){
		return isbn;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPublishedDate(PublishedDate publishedDate){
		this.publishedDate = publishedDate;
	}

	public PublishedDate getPublishedDate(){
		return publishedDate;
	}

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setCategories(List<String> categories){
		this.categories = categories;
	}

	public List<String> getCategories(){
		return categories;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setThumbnailUrl(String thumbnailUrl){
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getThumbnailUrl(){
		return thumbnailUrl;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setAuthors(List<String> authors){
		this.authors = authors;
	}

	public List<String> getAuthors(){
		return authors;
	}

	@Override
 	public String toString(){
		return 
			"BookItem{" + 
			"longDescription = '" + longDescription + '\'' + 
			",pageCount = '" + pageCount + '\'' + 
			",isbn = '" + isbn + '\'' + 
			",_id = '" + id + '\'' + 
			",publishedDate = '" + publishedDate + '\'' + 
			",shortDescription = '" + shortDescription + '\'' + 
			",categories = '" + categories + '\'' + 
			",title = '" + title + '\'' + 
			",thumbnailUrl = '" + thumbnailUrl + '\'' + 
			",status = '" + status + '\'' + 
			",authors = '" + authors + '\'' + 
			"}";
		}
}